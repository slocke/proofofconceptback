package com.example.poc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.core.io.Resource;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@RestController
public class PoCApplication {

	public static void main(String[] args) {
		SpringApplication.run(PoCApplication.class, args);
	}

	@PostMapping("/jobs")
	public ResponseEntity<String> postJob(@RequestBody(required = false) String args){
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN ,"*");
		try {

			//https://allgo18.inria.fr/api/v1/jobs
			HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.set("Content-Type","multipart/form-data");
			requestHeaders.set("Authorization","Token token=E5CEBGtSOMFiPBs9AItHUe4ehL5bK8GT");
			MultiValueMap<String,String > map = new LinkedMultiValueMap<String,String>();
			map.add("job[webapp_id]","1062");
			map.add("job[param]",args);
			map.add("job[queue]","standard");
			HttpEntity<MultiValueMap<String,String >> request = new HttpEntity<MultiValueMap<String,String >>(map,requestHeaders);
			RestTemplate template = new RestTemplate();
			System.out.println(request);
			ResponseEntity result = template.postForEntity("https://allgo18.inria.fr/api/v1/jobs",request,String.class);
			return ResponseEntity.ok().headers(responseHeaders).body(result.getBody().toString());
		} catch (Exception e){
			e.printStackTrace();
		}

		return ResponseEntity.badRequest().headers(responseHeaders).body("All goo g");
	}


	@GetMapping("/jobs/{id}")
	ResponseEntity<String> getJobs(@PathVariable(name = "id",required = true) int id){
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN ,"*");
		try {
			HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.set("Authorization","Token token=E5CEBGtSOMFiPBs9AItHUe4ehL5bK8GT");


			HttpEntity<Void> request = new HttpEntity<Void>(requestHeaders);
			RestTemplate template = new RestTemplate();
			ResponseEntity result = template.exchange("https://allgo18.inria.fr/api/v1/jobs/"+id, HttpMethod.GET,request,String.class);
			return ResponseEntity.ok().headers(responseHeaders).body(result.getBody().toString());
		} catch (Exception e){
			e.printStackTrace();
		}
		return ResponseEntity.ok().headers(responseHeaders).body("All goo g");
	}

	@GetMapping("/results/{id}")
	ResponseEntity<Object> getResult(@PathVariable(name = "id",required = true) int id){
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.set(HttpHeaders.ACCESS_CONTROL_ALLOW_ORIGIN ,"*");
		try {
			HttpHeaders requestHeaders = new HttpHeaders();
			requestHeaders.set("Authorization","Token token=E5CEBGtSOMFiPBs9AItHUe4ehL5bK8GT");
			HttpEntity<Void> request = new HttpEntity<Void>(requestHeaders);
			RestTemplate template = new RestTemplate();
			ResponseEntity result = template.exchange("https://allgo18.inria.fr/api/v1/datastore/" + id + "/result.txt", HttpMethod.GET,request,String.class);
			return ResponseEntity.ok().headers(responseHeaders).body(result.getBody());
		} catch (Exception e){
			e.printStackTrace();
		}
		return ResponseEntity.ok().headers(responseHeaders).body("All goo g");
	}


}
